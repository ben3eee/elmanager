using System.Reflection;
using System.Runtime.InteropServices;


// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

// Review the values of the assembly attributes

[assembly:AssemblyTitle("Elmanager")]
[assembly:AssemblyDescription("")]
[assembly:AssemblyCompany("")]
[assembly:AssemblyProduct("")]
[assembly:AssemblyCopyright("by Smibu")]
[assembly:AssemblyTrademark("")]

[assembly:ComVisible(false)]

//The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly:Guid("6814b76d-7083-4bd2-9b96-9e9c320830ba")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// <Assembly: AssemblyVersion("1.0.*")>

[assembly:AssemblyVersion("1.0.*")]
[assembly:AssemblyFileVersion("1.0.0.0")]

